#include <Wire.h>
#include <DHT.h>
#include <SoftwareSerial.h>

#define DS3231_I2C_ADDRESS 0x68
#define DHTTYPE DHT11

#define PIN_INPUT1 A0
#define PIN_INPUT2 A1
#define PIN_BLUETOOTH_RX 2
#define PIN_BLUETOOTH_TX 3
#define PIN_TEMPERATURE 4
#define PIN_MOTION1 5
#define PIN_MOTION2 6
#define PIN_MOTION3 7
#define PIN_BUZZER 8
#define PIN_RELAY1 12
#define PIN_RELAY2 11
#define PIN_RELAY3 13
#define PIN_RELAY4 10
#define PIN_RELAY5 9


#define COM_PING 9
#define COM_RELAY1_ON 10
#define COM_RELAY1_OFF 11
#define COM_RELAY5_ON 18
#define COM_RELAY5_OFF 19
#define COM_RELAY4_ON 12
#define COM_RELAY4_OFF 13
#define COM_RELAY3_ON 14
#define COM_RELAY3_OFF 15
#define COM_RELAY2_ON 16
#define COM_RELAY2_OFF 17

#define COM_GET_TEMPERATURE 1
#define COM_UPDATE_DATE 2
#define COM_GET_DATE 3
#define COM_GET_STATUS 4
#define COM_UPDATE_SCHEDULE 5
#define COM_UPDATE_STATUS 6

#define STATUS_ARMED 1
#define STATUS_DISARMED 0


DHT dht(PIN_TEMPERATURE, DHTTYPE);

SoftwareSerial BTserial(PIN_BLUETOOTH_RX, PIN_BLUETOOTH_TX); // RX | TX

int status = STATUS_DISARMED;

void setup() {

  BTserial.begin(9600);
  Serial.begin(9600);

  pinMode(PIN_INPUT1, INPUT);
  pinMode(PIN_INPUT2, INPUT);
  pinMode(PIN_MOTION1, INPUT);
  pinMode(PIN_MOTION2, INPUT);
  pinMode(PIN_MOTION3, INPUT);
  pinMode(PIN_BUZZER, OUTPUT);
  pinMode(PIN_RELAY1, OUTPUT);
  pinMode(PIN_RELAY2, OUTPUT);
  pinMode(PIN_RELAY3, OUTPUT);
  pinMode(PIN_RELAY4, OUTPUT);
  pinMode(PIN_RELAY5, OUTPUT);

  dht.begin();

  digitalWrite(PIN_BUZZER, LOW);

  scheduleTest(1);
}

void loop()
{
  bluetooth();
  //scheduler();
  scheduleTest(0);
  motion1();
  motion2();
  motion3();

  input1();
  input2();
}

byte in1 = 0;
byte in2 = 0;

void input1() {
  byte pInput  = digitalRead(PIN_INPUT1);
  byte *pSt = &in1;
  if (*pSt == 0 && pInput == HIGH) {
    BTserial.println("INPUT1#true");
    *pSt = 1;
  } else if (*pSt == 1 && pInput == LOW) {
    BTserial.println("INPUT1#false");
    *pSt = 0;
  }
}

void input2() {
  byte pInput  = digitalRead(PIN_INPUT2);
  byte *pSt = &in1;
  if (*pSt == 0 && pInput == HIGH) {
    BTserial.println("INPUT2#true");
    *pSt = 1;
  } else if (*pSt == 1 && pInput == LOW) {
    BTserial.println("INPUT2#false");
    *pSt = 0;
  }
}

int readFromBTSerial() {
  while (!BTserial.available()) {}
  return BTserial.read();
}

void scheduleTest(int setup) {

  /*if (setup == 1) {

    if (getDayOfWeek() == 1 || getDayOfWeek() == 2 || getDayOfWeek() == 3 || getDayOfWeek() == 4 || getDayOfWeek() == 5) {
      if (getHour() >= 10 && getHour() < 17) {
        status = STATUS_ARMED;
      } else {
        status = STATUS_DISARMED;
      }
    }

  } else {

    if (getHour() == 2 && getMinute() == 0) {
      status = STATUS_ARMED;
    }

    if (getHour() == 5 && getMinute() == 0) {
      status = STATUS_DISARMED;
    }

    if (getDayOfWeek() == 1 || getDayOfWeek() == 2 || getDayOfWeek() == 3 || getDayOfWeek() == 4 || getDayOfWeek() == 5) {

      if (getHour() == 10 && getMinute() == 0) {
        status = STATUS_ARMED;
      }
      if (getHour() == 17 && getMinute() == 0) {
        status = STATUS_DISARMED;
      }
    }
  }*/

  /*
     Lampu aquarium mati jam 2
  */
  if (setup == 1) {
    if (getHour() >= 18 || getHour() < 2) {
      digitalWrite(PIN_RELAY1, LOW);
    } else {
      digitalWrite(PIN_RELAY1, HIGH);
    }
  } else {
    if (getHour() == 18 && getMinute() == 0 && getSecond() == 0) {
      digitalWrite(PIN_RELAY1, LOW); // aquarium
    }
    if (getHour() == 2 && getMinute() == 0 && getSecond() == 0) {
      digitalWrite(PIN_RELAY1, HIGH); // aquarium
    }
  }

  /*
     Lampu luar
  */
  if (setup == 1) {
    if (getHour() >= 18 || getHour() < 6) {
      digitalWrite(PIN_RELAY3, LOW);
    } else {
      digitalWrite(PIN_RELAY3, HIGH);
    }
  } else {
    if (getHour() == 18 && getMinute() == 0 && getSecond() == 0) {
      digitalWrite(PIN_RELAY3, LOW); // lampu luar
    }
    if (getHour() == 6 && getMinute() == 0 && getSecond() == 0) {
      digitalWrite(PIN_RELAY3, HIGH); // lampu luar
    }
  }

  /*
     Lampu dalam & dapur
  */
  if (setup == 1) {
    if (getHour() >= 18) {
      digitalWrite(PIN_RELAY2, LOW);
      digitalWrite(PIN_RELAY4, LOW);
    } else {
      digitalWrite(PIN_RELAY2, HIGH);
      if (getHour() >= 2) {
        digitalWrite(PIN_RELAY4, HIGH);
      }
    }
  } else {
    if (getHour() == 0 && getMinute() == 0 && getSecond() == 0) {
      digitalWrite(PIN_RELAY2, HIGH); // lampu dalam
    }
    if (getHour() == 2 && getMinute() == 0 && getSecond() == 0) {
      digitalWrite(PIN_RELAY4, HIGH); // lampu dapur
    }
  }
}

void setDS3231time(byte second, byte minute, byte hour, byte dayOfWeek, byte
                   dayOfMonth, byte month, byte year)
{
  // sets time and date data to DS3231
  Wire.beginTransmission(DS3231_I2C_ADDRESS);
  Wire.write(0); // set next input to start at the seconds register
  Wire.write(decToBcd(second)); // set seconds
  Wire.write(decToBcd(minute)); // set minutes
  Wire.write(decToBcd(hour)); // set hours
  Wire.write(decToBcd(dayOfWeek)); // set day of week (1=Sunday, 7=Saturday)
  Wire.write(decToBcd(dayOfMonth)); // set date (1 to 31)
  Wire.write(decToBcd(month)); // set month
  Wire.write(decToBcd(year)); // set year (0 to 99)
  Wire.endTransmission();
}


/*void updateSchedules () {

  byte count = readFromBTSerial();

  Serial.print("Count: ");
  Serial.println(count);

  repo.remove("schedules");
  repo["schedules"] = jsonBuffer.createArray();

  JsonArray& schedules = repo["schedules"];
  for (byte i = 0; i < count; i++) {

    Serial.println();
    Serial.print("Schedule: ");
    Serial.println(i);

    JsonArray& schedule = jsonBuffer.createArray();
    for (byte x = 0; x <= 8; x++) {

      byte data = readFromBTSerial();

      Serial.print("Attr ");
      Serial.print(x);
      Serial.print(" = ");
      Serial.print(data);
      Serial.println();

      schedule.add(data);
    }
    schedules.add(schedule);
  }

  schedules.printTo(BTserial);
  BTserial.print(", Schedules updated");
  }

  void scheduler() {

  JsonArray& schedules = repo["schedules"];

  for (byte i = 0; i < schedules.size(); i++) {

    JsonArray& schedule = schedules[i];

    if (
          (schedule[0] == getSecond() || schedule[0] == 99) &&
          (schedule[1] == getMinute() || schedule[1] == 99) &&
          (schedule[2] == getHour() || schedule[2] == 99) &&
          (schedule[3] == getDayOfWeek() || schedule[3] == 99) &&
          (schedule[4] == getDayOfMonth() || schedule[4] == 99) &&
          (schedule[5] == getMonth() || schedule[5] == 99) &&
          (schedule[6] == getYear() || schedule[6] == 99)) {

      displayTime();
      if (schedule[7] == 1) {
        if (schedule[8] == 1) {
          BTserial.print(" Relay2 Scheduled On");
          digitalWrite(PIN_RELAY1, HIGH);
        } else {
          BTserial.print(" Relay2 Scheduled Off");
          digitalWrite(PIN_RELAY1, LOW);
        }
      } else if (schedule[7] == 2) {
        if (schedule[8] == 1) {
          BTserial.print(" Relay2 Scheduled On");
          digitalWrite(PIN_RELAY2, HIGH);
        } else {
          BTserial.print(" Relay2 Scheduled Off");
          digitalWrite(PIN_RELAY2, LOW);
        }
      } else if (schedule[7] == 3) {
        if (schedule[8] == 1) {
          BTserial.print(" Relay3 Scheduled On");
          digitalWrite(PIN_RELAY3, HIGH);
        } else {
          BTserial.print(" Relay3 Scheduled Off");
          digitalWrite(PIN_RELAY3, LOW);
        }
      } else if (schedule[7] == 4) {
        if (schedule[8] == 1) {
          BTserial.print(" Relay4 Scheduled On");
          digitalWrite(PIN_RELAY4, HIGH);
        } else {
          BTserial.print(" Relay4 Scheduled Off");
          digitalWrite(PIN_RELAY4, LOW);
        }
      }

      BTserial.println();

    }
  }
  }*/

void updateStatus () {
  status = readFromBTSerial();
  if (status == STATUS_DISARMED)
    BTserial.print("DISARMED!");
  else
    BTserial.print("ARMED!!!");
}

void updateSystemDate() {

  // DS3231 seconds, minutes, hours, day, date, month, year

  /*setDS3231time(
    BTserial.read(),
    BTserial.read(),
    BTserial.read(),
    BTserial.read(),
    BTserial.read(),
    BTserial.read(),
    BTserial.read());

    BTserial.print("Updated : ");
    displayTime();*/
  int seconds = readFromBTSerial();
  int minutes = readFromBTSerial();
  int hours = readFromBTSerial();
  int dow = readFromBTSerial();
  int dom = readFromBTSerial();
  int months = readFromBTSerial();
  int years = readFromBTSerial();

  setDS3231time(
    seconds,
    minutes,
    hours,
    dow,
    dom,
    months,
    years);
  BTserial.println("seconds:");
  BTserial.println(seconds);
  BTserial.println("minutes:");
  BTserial.println(minutes);
  BTserial.println("hours:");
  BTserial.println(hours);
  BTserial.println("dow:");
  BTserial.println(dow);
  BTserial.println("dom:");
  BTserial.println(dom);
  BTserial.println("months:");
  BTserial.println(months);
  BTserial.println("years:");
  BTserial.println(years);
  BTserial.print("System date updated");

}

byte st1 = 0;
byte st2 = 0;
byte st3 = 0;
void motion1() {
  byte *pSt = &st1;

  int pMotion = digitalRead(PIN_MOTION1);
  if (*pSt == 0 && pMotion == HIGH) {

    if (getHour() >= 18 || getHour() <= 6) {
      digitalWrite(PIN_RELAY2, LOW);
      digitalWrite(PIN_RELAY4, LOW);
    }
    BTserial.println("MOTION1#true");

    if (status == STATUS_ARMED) {
      digitalWrite(PIN_BUZZER, HIGH);
    }
    *pSt = 1;
  } else if (*pSt == 1 && pMotion == LOW) {

    if (getHour() >= 0 && getHour() <= 6) {
      digitalWrite(PIN_RELAY2, HIGH);
      digitalWrite(PIN_RELAY4, HIGH);
    }

    digitalWrite(PIN_BUZZER, LOW);
    BTserial.println("MOTION1#false");
    *pSt = 0;
  }
}


void motion2() {
  int pMotion  = digitalRead(PIN_MOTION2);
  byte *pSt = &st2;
  if (*pSt == 0 && pMotion == HIGH) {
    BTserial.println("MOTION2#true");
    *pSt = 1;
    digitalWrite(PIN_RELAY5, HIGH);
  } else if (*pSt == 1 && pMotion == LOW) {
    BTserial.println("MOTION2#false");
    *pSt = 0;
    digitalWrite(PIN_RELAY5, LOW);
  }

}


void motion3() {
  int pMotion = digitalRead(PIN_MOTION3);
  byte *pSt = &st3;
  if (*pSt == 0 && pMotion == HIGH) {
    BTserial.println("MOTION3#true");
    *pSt = 1;
  } else if (*pSt == 1 && pMotion == LOW) {
    BTserial.println("MOTION3#false");
    *pSt = 0;
  }

}

void getStatus() {
  BTserial.print("{\"temperatureAndHumidity\":");
  getTemperatureAndHumidity();
  BTserial.print(",");
  BTserial.print("\"relay\":{");
  BTserial.print("\"relay1\":");
  
  BTserial.flush();
  
  BTserial.print(digitalRead(PIN_RELAY1));
  BTserial.print(",\"relay2\":");
  BTserial.print(digitalRead(PIN_RELAY2));
  BTserial.print(",\"relay3\":");
  BTserial.print(digitalRead(PIN_RELAY3));
  BTserial.print(",\"relay4\":");
  
  BTserial.flush();
  
  BTserial.print(digitalRead(PIN_RELAY4));
  BTserial.print("},");
  BTserial.print("\"status\":");
  BTserial.print(status);
  BTserial.print("}");

}

void getTemperatureAndHumidity() {
  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  float f = dht.readTemperature(true);

  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t) || isnan(f)) {
    BTserial.print("Failed to read from DHT sensor!");
    return;
  }

  // Compute heat index in Fahrenheit (the default)
  float hif = dht.computeHeatIndex(f, h);
  // Compute heat index in Celsius (isFahreheit = false)
  float hic = dht.computeHeatIndex(t, h, false);

  BTserial.print("{\"temperature\":{");
  BTserial.print("\"celcius\":");
  BTserial.print(t);
  BTserial.print(",\"fahrenheit\":");
  BTserial.print(f);
  BTserial.print("},");
  BTserial.print("\"heatIndex\":{");
  BTserial.print("\"celcius\":");
  BTserial.print(hic);
  BTserial.print(",\"fahrenheit\":");
  BTserial.print(hif);
  BTserial.print("},");
  BTserial.print("\"humidity\":");
  BTserial.print(h);
  BTserial.print("}");
}



byte decToBcd(byte val)
{
  return ( (val / 10 * 16) + (val % 10) );
}
// Convert binary coded decimal to normal decimal numbers
byte bcdToDec(byte val)
{
  return ( (val / 16 * 10) + (val % 16) );
}

void readDS3231time(byte *second,
                    byte *minute,
                    byte *hour,
                    byte *dayOfWeek,
                    byte *dayOfMonth,
                    byte *month,
                    byte *year)
{
  Wire.begin();
  Wire.beginTransmission(DS3231_I2C_ADDRESS);
  Wire.write(0); // set DS3231 register pointer to 00h
  Wire.endTransmission();
  Wire.requestFrom(DS3231_I2C_ADDRESS, 7);
  // request seven bytes of data from DS3231 starting from register 00h
  *second = bcdToDec(Wire.read() & 0x7f);
  *minute = bcdToDec(Wire.read());
  *hour = bcdToDec(Wire.read() & 0x3f);
  *dayOfWeek = bcdToDec(Wire.read());
  *dayOfMonth = bcdToDec(Wire.read());
  *month = bcdToDec(Wire.read());
  *year = bcdToDec(Wire.read());
  Wire.end();
}

int getSecond() {
  byte second, minute, hour, dayOfWeek, dayOfMonth, month, year;
  readDS3231time(&second, &minute, &hour, &dayOfWeek, &dayOfMonth, &month, &year);
  return second;
}

int getMinute() {
  byte second, minute, hour, dayOfWeek, dayOfMonth, month, year;
  readDS3231time(&second, &minute, &hour, &dayOfWeek, &dayOfMonth, &month, &year);
  return minute;
}

int getHour() {
  byte second, minute, hour, dayOfWeek, dayOfMonth, month, year;
  readDS3231time(&second, &minute, &hour, &dayOfWeek, &dayOfMonth, &month, &year);
  return hour;
}

int getDayOfMonth() {
  byte second, minute, hour, dayOfWeek, dayOfMonth, month, year;
  readDS3231time(&second, &minute, &hour, &dayOfWeek, &dayOfMonth, &month, &year);
  return dayOfMonth;
}

int getDayOfWeek() {
  byte second, minute, hour, dayOfWeek, dayOfMonth, month, year;
  readDS3231time(&second, &minute, &hour, &dayOfWeek, &dayOfMonth, &month, &year);
  return dayOfWeek;
}

int getMonth() {
  byte second, minute, hour, dayOfWeek, dayOfMonth, month, year;
  readDS3231time(&second, &minute, &hour, &dayOfWeek, &dayOfMonth, &month, &year);
  return month;
}

int getYear() {
  byte second, minute, hour, dayOfWeek, dayOfMonth, month, year;
  readDS3231time(&second, &minute, &hour, &dayOfWeek, &dayOfMonth, &month, &year);
  return year;
}

void displayTime()
{
  byte second, minute, hour, dayOfWeek, dayOfMonth, month, year;
  // retrieve data from DS3231
  readDS3231time(&second, &minute, &hour, &dayOfWeek, &dayOfMonth, &month,
                 &year);
  // send it to the serial monitor
  BTserial.print(hour, DEC);
  // convert the byte variable to a decimal number when displayed
  BTserial.print(":");
  if (minute < 10)
  {
    BTserial.print("0");
  }
  BTserial.print(minute, DEC);
  BTserial.print(":");
  if (second < 10)
  {
    BTserial.print("0");
  }
  BTserial.print(second, DEC);
  BTserial.print(" ");
  BTserial.print(dayOfMonth, DEC);
  BTserial.print("/");
  BTserial.print(month, DEC);
  BTserial.print("/");
  BTserial.print(year, DEC);
  BTserial.print(" Day of week: ");
  switch (dayOfWeek) {
    case 1:
      BTserial.print("Sunday");
      break;
    case 2:
      BTserial.print("Monday");
      break;
    case 3:
      BTserial.print("Tuesday");
      break;
    case 4:
      BTserial.print("Wednesday");
      break;
    case 5:
      BTserial.print("Thursday");
      break;
    case 6:
      BTserial.print("Friday");
      break;
    case 7:
      BTserial.print("Saturday");
      break;
  }
}

void bluetooth() {
  if (BTserial.available()) {
    int data = BTserial.read();

    if (data == COM_PING) {
      BTserial.print("PING");
    } else if (data == COM_RELAY1_OFF) {
      digitalWrite(PIN_RELAY1, HIGH);
    } else if (data == COM_RELAY1_ON) {
      digitalWrite(PIN_RELAY1, LOW);
    } else if (data == COM_RELAY2_OFF) {
      digitalWrite(PIN_RELAY2, HIGH);
    } else if (data == COM_RELAY2_ON) {
      digitalWrite(PIN_RELAY2, LOW);
    } else if (data == COM_RELAY3_OFF) {
      digitalWrite(PIN_RELAY3, HIGH);
    } else if (data == COM_RELAY3_ON) {
      digitalWrite(PIN_RELAY3, LOW);
    } else if (data == COM_RELAY4_OFF) {
      digitalWrite(PIN_RELAY4, HIGH);
    } else if (data == COM_RELAY4_ON) {
      digitalWrite(PIN_RELAY4, LOW);
    } else if (data == COM_RELAY5_OFF) {
      digitalWrite(PIN_RELAY5, LOW);
    } else if (data == COM_RELAY5_ON) {
      digitalWrite(PIN_RELAY5, HIGH);
    } else if (data == COM_GET_TEMPERATURE) {
      getTemperatureAndHumidity();
    } else if (data == COM_GET_STATUS) {
      getStatus();
    } else if (data == COM_UPDATE_DATE) {
      updateSystemDate();
    } else if (data == COM_GET_DATE) {
      displayTime();
    } else if (data == COM_UPDATE_STATUS) {
      updateStatus();
    } else if (data == COM_UPDATE_SCHEDULE) {
      //      updateSchedules();
    } else {
      BTserial.print("Unknown command");
    }

    BTserial.print("#");
    BTserial.print(data);
    BTserial.println();
  }

  if (Serial.available()) {
    BTserial.print(Serial.readString());
    BTserial.println();
  }
}

